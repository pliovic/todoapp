﻿using System;
namespace Shared.Models
{
    public class DataTableFilter
    {
        public int Current { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public bool Ascending { get; set; }
        public string OrderBy { get; set; }
    }

    public class TodoTableFilter : DataTableFilter
    {
        public string StatusFilter { get; set; }
    }
}
