﻿using System;
namespace Bll.Models
{
    public class TodoTableRowModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }

    }
}
