﻿namespace Bll.Models
{
    public class DataAndCount
    {
        public object Data { get; set; }
        public int Total { get; set; }
    }
}
