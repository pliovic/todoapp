﻿using System.Threading.Tasks;
using Bll.Models;
using Dal.Models.Entities;
using Shared.Models;

namespace Bll.Interfaces
{
    public interface ITodoService
    {
        Task<DataAndCount> GetFiltered(TodoTableFilter filter);
        Task<TodoItem> GetByIdAsync(int id);
        Task<TodoItem> Create(TodoItem model);
    }
}
