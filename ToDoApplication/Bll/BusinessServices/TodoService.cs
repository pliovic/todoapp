﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dal;
using Dal.Models.Entities;
using Bll.Interfaces;
using Microsoft.EntityFrameworkCore;
using Bll.Models;
using Shared.Models;
using System.Linq;
using AutoMapper;

namespace Bll.BusinessServices
{
    public class TodoService : ITodoService
    {
        private readonly IToDoApplicationDbContext _toDoApplicationDbContext;
        private readonly IMapper _mapper;

        public TodoService(IToDoApplicationDbContext toDoApplicationDbContext, IMapper mapper)
        {
            _toDoApplicationDbContext = toDoApplicationDbContext;
            _mapper = mapper;
        }

        public async Task<DataAndCount> GetFiltered(TodoTableFilter filter)
        {
            var query = _toDoApplicationDbContext.TodoItemRepository
                .Get();

            if (!string.IsNullOrWhiteSpace(filter.OrderBy))
                query = filter.Ascending ? query.OrderBy(x => x.Name) : query.OrderByDescending(x => x.Name);

            if (!string.IsNullOrWhiteSpace(filter.StatusFilter) && filter.StatusFilter != "All")
            {
                var isComplete = filter.StatusFilter == "Complete";
                query = query.Where(x => x.IsComplete == isComplete);
            }

            var items = await query.Skip((filter.Current - 1) * filter.PageSize).Take(filter.PageSize).ToListAsync();

            var tableRowItems = _mapper.Map<List<TodoItem>, List<TodoTableRowModel>>(items);
            return new DataAndCount
            {
                Data = tableRowItems,
                Total = query.Count()
            };
        }

        public async Task<TodoItem> GetByIdAsync(int id)
        {
            return await _toDoApplicationDbContext.TodoItemRepository
                .Get()
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<TodoItem> Create(TodoItem model)
        {
            _toDoApplicationDbContext.TodoItemRepository.Add(model);
            await _toDoApplicationDbContext.SaveChangesAsync();
            return model;
        }
    }
}
