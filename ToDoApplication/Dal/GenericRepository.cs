﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;

namespace Dal
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbSet<TEntity> dbSet)
        {
            _dbSet = dbSet;
        }

        public TEntity Add(TEntity entity) => _dbSet.Add(entity).Entity;

        public void AddRange(IEnumerable<TEntity> entities) => _dbSet.AddRange(entities);

        public void AddRange(params TEntity[] entities) => _dbSet.AddRange(entities);

        public bool Any()
        {
            return _dbSet.Any();
        }

        public TEntity Attach(TEntity entity) => _dbSet.Attach(entity).Entity;

        public void AttachRange(IEnumerable<TEntity> entities) => _dbSet.AttachRange(entities);

        public void AttachRange(params TEntity[] entities) => _dbSet.AttachRange(entities);

        public TEntity Find(params object[] keyValues) => _dbSet.Find(keyValues);

        public ValueTask<TEntity> FindAsync(object[] keyValues, CancellationToken cancellationToken) => _dbSet.FindAsync(keyValues, cancellationToken);

        public ValueTask<TEntity> FindAsync(params object[] keyValues) => _dbSet.FindAsync(keyValues);

        public IQueryable<TEntity> Get(bool tracking = false)
        {
            return tracking ? _dbSet.AsQueryable() : _dbSet.AsNoTracking();
        }

        public TEntity Remove(TEntity entity) => _dbSet.Remove(entity).Entity;

        public void RemoveRange(IEnumerable<TEntity> entities) => _dbSet.RemoveRange(entities);

        public void RemoveRange(params TEntity[] entities) => _dbSet.RemoveRange(entities);

        public TEntity Update(TEntity entity) => _dbSet.Update(entity).Entity;

        public void UpdateRange(IEnumerable<TEntity> entities) => _dbSet.UpdateRange(entities);

        public void UpdateRange(params TEntity[] entities) => _dbSet.UpdateRange(entities);
    }
}
