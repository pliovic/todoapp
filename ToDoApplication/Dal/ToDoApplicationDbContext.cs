﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dal.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Dal
{
    public class ToDoApplicationDbContext : DbContext, IToDoApplicationDbContext
    {
        public IRepository<TodoItem> TodoItemRepository => GetRepository<TodoItem>();

        public DbSet<TodoItem> TodoItems { get; set; }

        public ToDoApplicationDbContext(DbContextOptions<ToDoApplicationDbContext> options) : base(options)
        {

        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            var returnInt = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

            try
            {
                await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.ToString());
            }

            return returnInt;
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new GenericRepository<TEntity>(Set<TEntity>());
        }
    }
}
