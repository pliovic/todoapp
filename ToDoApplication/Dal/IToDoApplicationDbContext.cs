﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Dal.Models.Entities;

namespace Dal
{
    public interface IToDoApplicationDbContext : IDisposable
    {
        // repositories
        IRepository<TodoItem> TodoItemRepository { get; }

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
