﻿using Moq;
using NUnit.Framework;
using Dal;
using Dal.Models.Entities;
using System.Linq;
using MockQueryable.Moq;
using System.Collections.Generic;
using Bll.BusinessServices;
using System.Threading.Tasks;
using FluentAssertions;
using AutoMapper;
using Shared.Models;
using ToDoApplication.Profiles;


namespace Bll.Tests.Services
{
    [TestFixture]
    public class TodoServiceTest
    {
        private MockRepository _mockRepository;

        private Mock<IToDoApplicationDbContext> _mockToDoApplicationDbContext;
        private Mock<IRepository<TodoItem>> _mockDbRepository;
        public IMapper _mapper;

        private IQueryable<TodoItem> _todoItems = new List<TodoItem>().AsQueryable();

        [SetUp]
        public void SetUp()
        {
            _mockRepository = new MockRepository(MockBehavior.Loose);

            _mockToDoApplicationDbContext = _mockRepository.Create<IToDoApplicationDbContext>();
            _mockDbRepository = _mockRepository.Create<IRepository<TodoItem>>();

            _mockDbRepository.Setup(i => i.Get(It.IsAny<bool>())).Returns(_todoItems.BuildMock().Object);
            _mockToDoApplicationDbContext.Setup(i => i.TodoItemRepository).Returns(_mockDbRepository.Object);
            _mockToDoApplicationDbContext.Setup(i => i.TodoItemRepository.Add(It.IsAny<TodoItem>())).Callback<TodoItem>(item =>
            {
                var tmp = _todoItems.ToList();
                tmp.Add(item);
                _todoItems = tmp.AsQueryable();
            }).Returns<TodoItem>(item => item);

            var config = new MapperConfiguration(opts =>
            {
                opts.AddProfile<TodoItemProfile>();
            });
            _mapper = config.CreateMapper();
        }

        private TodoService CreateService()
        {
            _todoItems = GetTodoItems;

            SetUp();

            return new TodoService(_mockToDoApplicationDbContext.Object, _mapper);
        }

        [Test]
        public async Task GetAllTodos_WithFilter_ExactNumberOfCompletedTasks()
        {
            // Arrange
            var _service = CreateService();
            var filter = new TodoTableFilter()
            {
                Ascending = false,
                Current = 1,
                PageSize = 10,
                StatusFilter = "Complete",
                OrderBy = "null"
            };
            var countedTodoItemsWithCompletedTasks = GetTodoItems.Where(x => x.IsComplete).Count();

            // Act
            var result = await _service.GetFiltered(filter);

            // Assert
            result.Total.Should().Be(countedTodoItemsWithCompletedTasks);
        }

        [Test]
        public async Task GetAllTodos_NoFilter_ExactNumberOfNotCompletedTasks()
        {
            // Arrange
            var _service = CreateService();
            var filter = new TodoTableFilter();
            var countedTodoItemsWithNoFilter = GetTodoItems.Count();

            // Act
            var result = await _service.GetFiltered(filter);

            // Assert
            result.Total.Should().Be(countedTodoItemsWithNoFilter);
        }

        [Test]
        public async Task GetAllTodos_WithFilter_ExactNumberOfNotCompletedTasks()
        {
            // Arrange
            var _service = CreateService();
            var filter = new TodoTableFilter()
            {
                Ascending = false,
                Current = 1,
                PageSize = 10,
                StatusFilter = "NotCompleded",
                OrderBy = "null"
            };
            var countedTodoItemsWithNotCompletedTasks = GetTodoItems.Where(x => !x.IsComplete).Count();

            // Act
            var result = await _service.GetFiltered(filter);

            // Assert
            result.Total.Should().Be(countedTodoItemsWithNotCompletedTasks);
        }

        [Test]
        public async Task GetById_ExistingIdPassed_ResultIsValidItem()
        {
            // Arrange
            var testId = 1;
            var todoItem = GetTodoItems.FirstOrDefault(x => x.Id == testId);
            var _service = CreateService();

            // Act
            var result = await _service.GetByIdAsync(testId);

            // Assert
            result.Id.Should().Be(todoItem.Id);
        }

        [Test]
        public async Task GetById_NotExistingIdPassed_ResultIsNull()
        {
            // Arrange
            var testId = 10;
            var _service = CreateService();

            // Act
            var result = await _service.GetByIdAsync(testId);

            // Assert
            result.Should().BeNull();
        }

        [Test]
        public async Task Create_ValidInputData__TodoItemCreated()
        {
            // Arrange
            var service = CreateService();
            var itemToInsert = new TodoItem()
            {
                Id = 6,
                Name = "Test Create",
                IsComplete = true
            };

            // Act
            await service.Create(itemToInsert);

            // Assert
            var insertedItem = _todoItems.FirstOrDefault(x => x.Id == itemToInsert.Id);
            Assert.AreEqual(itemToInsert, insertedItem);
        }


        private IQueryable<TodoItem> GetTodoItems => new List<TodoItem>()
        {
            new TodoItem(){Id = 1, Name = "Task1", IsComplete = true},
            new TodoItem(){Id = 2, Name = "Task2", IsComplete = false},
            new TodoItem(){Id = 3, Name = "Task3", IsComplete = true},
            new TodoItem(){Id = 4, Name = "Task4", IsComplete = false},
            new TodoItem(){Id = 5, Name = "Task5", IsComplete = true},


        }.AsQueryable();
    }
}
