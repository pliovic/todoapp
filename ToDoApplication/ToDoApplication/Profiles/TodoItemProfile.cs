﻿using Dal.Models.Entities;
using AutoMapper;
using Bll.Models;
using ToDoApplication.Models;

namespace ToDoApplication.Profiles
{
    public class TodoItemProfile : Profile
    {
        public override string ProfileName => nameof(TodoItemProfile);

        public TodoItemProfile()
        {
            CreateMap<TodoItemDto, TodoItem>().ReverseMap();
            CreateMap<TodoTableRowModel, TodoItem>().ReverseMap();
        }
    }
}
