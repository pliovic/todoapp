using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Dal;
using Bll.BusinessServices;
using Microsoft.EntityFrameworkCore;
using Bll.Interfaces;
using AutoMapper;
using System;

namespace ToDoApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<ToDoApplicationDbContext>(optionsBuilder => optionsBuilder.UseNpgsql(
                    Configuration.GetConnectionString("ConnectionString")
                    ));

            services.AddTransient<IToDoApplicationDbContext, ToDoApplicationDbContext>();
            services.AddScoped<ITodoService, TodoService>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
