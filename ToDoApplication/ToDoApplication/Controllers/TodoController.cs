﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Dal.Models.Entities;
using ToDoApplication.Models;
using Bll.Interfaces;
using AutoMapper;
using Shared.Models;

namespace ToDoApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodoController : ControllerBase
    {
        private readonly ITodoService _todoservice;
        private readonly IMapper _mapper;

        public TodoController(ITodoService todoService, IMapper mapper)
        {
            _todoservice = todoService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetFiltered([FromQuery]TodoTableFilter filter)
        {
            var data = await _todoservice.GetFiltered(filter);
            return Ok(data);
        }

        [HttpGet("{id}", Name = "GetTodoItem")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var contact = await _todoservice.GetByIdAsync(id);
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TodoItemDto todoItemDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var boItem = _mapper.Map<TodoItem>(todoItemDto);

            var newItem = await _todoservice.Create(boItem).ConfigureAwait(false);

            return CreatedAtRoute("GetTodoItem", new { id = newItem.Id }, _mapper.Map<TodoItemDto>(newItem));
        }
    }
}
